jQuery(function($){
	$(document).ready(function() {					
		$('#mainImage').jQLiteCycle({
			delay: 3500,
			transitionSpeed: 1700,
			fit: 'cover',
			lazyLoad:'$slides'
		});
	});
});
