
if(jQuery) {
		// tkiGallery - v.0.1.0
	(function($) {
		var tkiGalleryClass = function() {
			// -------- Object properties --------
			var self = this;
			var error = false;
			// Settings
			this.settings = {
				'viewerSelector' : null,
				'initialImage' : null
			};
			// jQuery objects
			var $viewer;
			var $thumbsHolder;
			var $thumbLinks;
			var $thumbs;

			// -------- Object methods --------
			this.methods = {
				// Init
				init : function(options) {
					// Merge custom settings
					$.extend(self.settings,options); 
					// Check settings
					if(!self.settings.viewerSelector) {
						self.methods.errorLog('viewerSelector not set');
						return;
					}
					// Find viewer
					$viewer = $(self.settings.viewerSelector);

					// Find thumbs holder, thumb links, and thumbs
					$thumbsHolder = $(this);
					$thumbLinks = $thumbsHolder.children('a');
					$thumbs = $thumbsHolder.find('img');
					$thumbs.addClass('tkiGalleryThumb');
					
					// Set initial image if not specified
					if(!self.settings.initialImage) {
						self.settings.initialImage = $viewer.children('img').attr('src');
					}
					// Select initial thumb
					$thumbLinks.each(function(i) {
						if(this.getAttribute('href') === self.settings.initialImage) {
							$('img',this).addClass('tkiSelected');
						}
					});
					// Thumbs events
					$thumbLinks.click(function(evt) {
						evt.preventDefault();
						self.methods.updateThumbs(this);
						self.methods.switchImage(this);
					});
				},
				updateThumbs : function(el) {
					$thumbs.removeClass('tkiSelected');
					$('img',el).addClass('tkiSelected');
				},
				switchImage : function(el) {
					$viewer.children('img').attr('src',el.getAttribute('href'));
				},
				errorLog : function(err) {
					if(err) {
						console.log('tkiSwitcher error: ' + err);
					}
				}
			};
		};

		$.fn.tkiGallery = function() {
			var args = arguments; //Array.prototype.slice.call(arguments);

			//console.log('arg.length: ' + arg.length);
			var gallery = new tkiGalleryClass();
			// Method logic
			if(gallery.methods[args[0]]) {
				var otherArgs = Array.prototype.slice.call(args,1);
				return this.each(function() {
					gallery.methods[args[0]].apply(this,otherArgs);
				});
			} else if (typeof args[0] === 'object' || !args) {
				return this.each(function() {
					gallery.methods.init.apply(this,args);
				});
			} else {
				gallery.methods.errorLog('No arguments specified for plugin');
			}
		};
	})(jQuery);
	// -------- Init --------
		$(document).ready(function() {
			$('#tkiGalleryThumbs').tkiGallery({'viewerSelector':'#tkiGalleryViewer'});
		});
}