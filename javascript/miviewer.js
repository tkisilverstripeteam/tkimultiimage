		// Image switcher - v.0.1.0
;(function($) {
	var tkiMiViewerClass = function() {
		// -------- Object properties --------
		var self = this;
		var error = false;
		// Settings
		this.settings = {
			'viewerMain' : '.MiViewerMain',
			'initialImage' : null
		};
		// jQuery objects
		var $viewerMain;
		var $thumbsHolder;
		var $thumbLinks;
		var $thumbs;

		// -------- Object methods --------
		this.methods = {
			// Init
			init : function(options) {
				// Merge custom settings
				$.extend(self.settings,options); 
				// Check settings
				if(!self.settings.viewerMain) {
					self.methods.errorLog('viewerMain not set');
					return;
				}
				// Find viewer
				$viewerMain = $(self.settings.viewerMain);
				// Find thumbs holder, thumb links, and thumbs
				$thumbsHolder = $(this);
				$thumbLinks = $thumbsHolder.children('a');
				$thumbs = $thumbsHolder.find('img');
				$thumbs.addClass('MiViewerNavItem');

				// Set initial image if not specified
				if(!self.settings.initialImage) {
					self.settings.initialImage = $viewerMain.children('img').attr('src');
				}
				// Select initial thumb
				$thumbLinks.each(function(i) {
					if(this.getAttribute('href') === self.settings.initialImage) {
						$('img',this).addClass('MiSelected');
					}
				});
				// Thumbs events
				$thumbLinks.click(function(evt) {
					evt.preventDefault();
					self.methods.updateThumbs(this);
					self.methods.switchImage(this);
				});
			},
			updateThumbs : function(el) {
				$thumbs.removeClass('MiSelected');
				$('img',el).addClass('MiSelected');
			},
			switchImage : function(el) {
				$viewerMain.children('img').attr('src',el.getAttribute('href'));
			},
			errorLog : function(err) {
				if(err) {
					console.log('tkiMiViewer error: ' + err);
				}
			}
		};
	};

	$.fn.tkiMiViewer = function() {
		var args = arguments; //Array.prototype.slice.call(arguments);
		
		//console.log('args.length: ' + args.length);
		var viewer = new tkiMiViewerClass();
		// Method logic
		if(viewer.methods[args[0]]) {
			var otherArgs = Array.prototype.slice.call(args,1);
			return this.each(function() {
				viewer.methods[args[0]].apply(this,otherArgs);
			});
		} else if (typeof args[0] === 'object' || args.length === 0) {
			return this.each(function() {
				viewer.methods.init.apply(this,args);
			});
		} else {
			viewer.methods.errorLog('Did not initialize');
		}
	};
})(jQuery);
