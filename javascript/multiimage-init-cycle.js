/* multiimage init script
 * Copyright (c) 2013 Todd Hossack / todd(at)tiraki.com
 * Last updated: 2013-05-30
 */

if(jQuery) {
	;(function($) {
		$(document).ready(function() {
			function TkiNaturalImageDim(img) {
				var t = new Image();
				t.src = (img.getAttribute ? img.getAttribute("src") : false) || img.src;
				return Array(t.width,t.height);
			}
			
			$(".micycle").each(function() {
				
				var $miHolder = $(this).children('.miholder');
				// Expand container height to fit largest child,
				// because children are absolutely positioned
				var miItemMaxH = 0;
				$miHolder.children().each(function() {
					var itemH = 0;
					
					$(this).children().each(function() {
						var elH = 0;
						var tag = this.nodeName.toLowerCase()
						if(tag === 'img') {
							elH = $(this).attr('height') || $(this).outerHeight() || $(this).height();
						} else {
							elH = $(this).outerHeight() || $(this).height();
						}
						//console.log(tag);
						//console.log(elH);
						if(elH > 0) itemH += +elH;
					});
					if(itemH > miItemMaxH) miItemMaxH = itemH;
				});
				$miHolder.css("height",miItemMaxH + 'px');
				//console.log(miItemMaxH);
				
					// Cycle
				$miHolder.cycle({
					autostop: 0,
					containerResize: 1,
					containerResizeHeight: 0,
					slideResize: 0,
					fit: 0
				});
			});

		});
	})(jQuery);
}