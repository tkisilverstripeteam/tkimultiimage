<?php

// Check dependencies
//SortableDataObject::add_sortable_class("MultiImage");

Object::add_extension('Image','MultiImageExtension');

/* Output types */
MultiImagePageExtension::$multi_image_types = array(
	 'static' => array(
		'Title' => 'Static - Single or multiple images'
	),
	'random' => array(
		'Title' => 'Random - Single image'
	),
	'cycle' => array(
		'Title' => 'Slideshow - Cycles through images'
	)
);

MultiImagePageExtension::$multi_image_positions = array(
	 'before' => array(
		'Title' => 'Before content'
		//'Icon' => ''
	),
	'after' => array(
		'Title' => 'After content'
		//'Icon' => ''
	)
);
 
/* Example configuration for /mysite/_config.php

Object::add_extension('Page','MultiImagePageExtension');
Object::add_extension('Page','TkiDoVersioning');	// Versioning for multi images
Object::add_extension('Page','BgImagePageExtension');

// Image size presets 
MultiImagePageExtension::$default_image = 'assets/Uploads/defaults/noimage.gif';
MultiImageExtension::add_preset('large',1024,1024,false,90);
MultiImageExtension::add_preset('medium',600,600,false,90);
MultiImageExtension::add_preset('small',240,240,true,80);
MultiImageExtension::add_preset('thumbnail',60,60,true,80);
*/