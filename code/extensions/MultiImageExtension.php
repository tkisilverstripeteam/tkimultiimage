<?php
/**
 */
class MultiImageExtension extends DataExtension {
	
	protected static $default_cropped = false;
	
	protected static $default_quality = 90;
	
	protected static $default_preset = 'medium';
	
	protected static $presets = array(
		'large' => array('width' => 1024,'height' => 1024,'quality' => 90,'cropped' => false),
		'medium' => array('width' => 600,'height' => 600,'quality' => 90,'cropped' => false),
		'small' => array('width' => 300,'height' => 300,'quality' => 90,'cropped' => false),
		'thumbnail' => array('width' => 100,'height' => 100,'quality' => 90,'cropped' => false),
	);

	/* ---- Instance variables ---- */
	
	/* ---- Static methods ---- */
		// Deprecated: use add_preset method
	public static function set_default_size($name,$width=0,$height=0,$cropped=false,$quality=90) {
		return self::add_preset($name,$width,$height,$cropped,$quality);
	}
	
	protected static function clean_dimension($dim) {
		return preg_replace('/[^0-9m]/','',strtolower(strval($dim)));
	}
	public static function add_preset($name,$width=0,$height=0,$cropped=false,$quality=90) {
		$name = (string) $name;
		$width = self::clean_dimension($width);
		$height = self::clean_dimension($height);
		$quality = (int) $quality;
		$cropped = (bool) $cropped;
		if(!empty($name) && (!empty($width) || !empty($height))) {
			self::$presets[$name] = array(
				'width' => $width,
				'height' => $height,
				'quality' => $quality,
				'cropped' => $cropped
			);
		} else {
			return false;
		}
	}

	public static function get_presets() {
		return self::$presets;
	}
	
	public static function get_preset($name) {
		return array_key_exists($name,self::$presets) ? self::$presets[$name] : null;
	}
	
	public static function set_default_preset($name) {
		if(self::preset_exists($name)) self::$default_preset = $name;
	}
	/* 
	 * Returns the default preset or if there's no default
	 */
	public static function get_default_preset() {
		$default = !empty(self::$default_preset) ? self::$default_preset : 0;
		return array_key_exists($default,self::$presets) ? self::$presets[$default] : null;
	}
	
	public static function preset_exists($name) {
		$out = false;
		if(is_string($name) && !empty($name)) {
			$out = array_key_exists($name,self::$presets);
		}
		return $out;
	}
	
	public static function set_default_quality($num) {
		self::$default_quality = (int) $num;
	}
	
	public static function get_default_quality() {
		return self::$default_quality;
	}
	
	public static function set_default_cropped($v) {
		self::$default_cropped = (bool) $v;
	}
	
	public static function get_default_cropped() {
		return self::$default_cropped;
	}
	
	public static function set_default_fit($v) {
		if($v === 'width' || $v === 'height') {
			self::$default_fit = $v;
		}
	}
	/* Checks and creates the correct keys and casted values for image formmatting arguments
	 * return array
	 */
	public static function get_image_args($preset) {
		$specs = array(
			'width' => array_key_exists('width',$preset) ? $preset['width'] : 0,
			'height' => array_key_exists('height',$preset) ? $preset['height'] : 0,
			'quality' => array_key_exists('quality',$preset) ? (int) $preset['quality'] : (int) self::get_default_quality(),
			'cropped' => array_key_exists('cropped',$preset) ? (bool) $preset['cropped'] : (bool) self::get_default_cropped()
		);
		return $specs;
	}

	/* ---- Instance methods ---- */

	// Template method
	public function PresetImage($name) {
		if(!array_key_exists($name,self::$presets)) return null;
		return $this->MiImage(self::$presets[$name]);
	}
	// Deprecated method
	public function generatePresetImage($gd,$key) {
			// Check if the preset is in the array
		if(!array_key_exists($key,self::$presets)) {
			return null;
		}
			// Set quality
		$gd->setQuality(self::$presets[$key]['quality']);
			// Resized and cropped to fit within the dimensions
		if(self::$presets[$key]['cropped']) {
			return $gd->croppedResize(
				self::$presets[$key]['width'],
				self::$presets[$key]['height']
			);
		} else {
			// Resized to fit in the dimensions - not cropped or padded
			return $this->resizeByWidthOrHeight($gd,
				self::$presets[$key]['width'],
				self::$presets[$key]['height']
			);
		}
	}
	
		// Internal method
	public function MiImage($args) {
		$specs = self::get_image_args($args);
		$dims = array('width' => array_shift($specs),'height' => array_shift($specs));
		return $this->owner->getFormattedImage('MiImage',$dims,$specs);
	}
	// Internal method
	public function generateMiImage($gd,$dims,$specs) {
		$gd->setQuality($specs['quality']);

		if($specs['cropped'] && (!empty($dims['width']) && !empty($dims['height']))) {
			return $gd->croppedResize(intval($dims['width']),intval($dims['height']));
		} else {
			return $this->resizeByWidthOrHeight($gd,$dims['width'],$dims['height']);
		}
	}
	/* Image manipulation methods */
	protected function resizeByWidthOrHeight($gd,$width=null,$height=null) {
		$image = null;
		if(empty($width) && empty($height)) return null;
			// To do: pre-determine resizeByHeight or resizeByWidth, as fittedResize
			// is not efficient
		if(!empty($width) && !empty($height)) {
			if(substr(strval($width),-1) === 'm' || substr(strval($height),-1) === 'm') {
				$image = $gd->fittedResize(intval($width),intval($height));
				return $image;
			}
			$orientation = $this->owner->getOrientation();
		} else {
			$orientation = !empty($width) ? 2 : 1;
		}
		
		switch($orientation) {
			case 1: 
				$image = $gd->resizeByHeight($height);
				break;
			case 0:
			case 2:
			default:
				$image = $gd->resizeByWidth($width);
				break;
		}
		return $image;
	}

}



