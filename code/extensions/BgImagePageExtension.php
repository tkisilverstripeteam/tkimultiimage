<?php
/**
 */
class BgImagePageExtension extends MultiImagePageExtension {
	
	public static $default_image = '';
	public static $multi_image_limit = 100;
	
	private static $db = array(
		'BgSlideshow' => 'Boolean',
		'BgRandom' => 'Boolean'
	);
	
	private static $has_many = array(
		'BackgroundImages' => 'BackgroundImage'
	);
	
	function updateCMSFields(FieldList $fields) {
		
		if(!$fields->dataFieldByName('BackgroundImages')) {
			$image_mgr = new GridField(
				'BackgroundImages',
				'BackgroundImage',
				BackgroundImage::get()
			);
			if($this->owner->BackgroundImages()->count() > self::$multi_image_limit) {
				//$image_mgr->removePermission('add');
			}
			//$image_mgr->copyOnImport = false; 
			$fields->addFieldToTab('Root.Main',$image_mgr);
			
			$fields->addFieldToTab('Root.Main',
				new CheckboxField('BgSlideshow','Slideshow',0),
				'BackgroundImages'
			);
			$fields->addFieldToTab('Root.Main',
				new CheckboxField('BgRandom','Random Image',0),
				'BackgroundImages'
			);
		}
	}
	
	public function FindBackgroundImage() {
		$out = null;
		$image = null;
		$lazyLoadStr = '';
			// Get images
		$imageSet = $this->owner->BackgroundImages();
		if($imageSet->exists()) {
				// Convert to array
			$bgImages = $imageSet->toArray();
			if($this->owner->BgRandom) {
				shuffle($bgImages);
			}
				// Load first image
			$image = array_shift($bgImages);
			if(!MultiImageExtension::preset_exists('bgslideshow')) {
				MultiImageExtension::add_preset('bgslideshow',1280,960,true,60);
			}
			// If slideshow is enabled
			if($this->owner->BgSlideshow) {
				if(!empty($bgImages)) {
					foreach($bgImages as $bgImg) {
						$formattedImg = $bgImg->Image()->PresetImage('bgslideshow');
						if($formattedImg) {
							$lazyLoadStr .= '<img src="'.$formattedImg->URL.'" alt="" />';
						}
					}
				}
			}
			$out = ($image) ? $image->Image()->PresetImage('bgslideshow') : null;
		} elseif(!empty(self::$default_image)) {
			$file = Convert::raw2sql(self::$default_image);
			$image = Image::get_one('Image',"\"Filename\"='$file'");
			$out = ($image) ? $image : null;
		}
		// Still require these for single/random background images, as they take
		// care of image resizing
		//Requirements::javascript('tkimultiimage/thirdparty/jqlite-cycle-uber-lite-custom.js');
		Requirements::javascriptTemplate(
			'tkimultiimage/javascript/bgslideshow.js',
			array('slides'=>$lazyLoadStr)
		);
		return $out;
	}

	public function BackgroundSlideshow() {
		
	}
}



