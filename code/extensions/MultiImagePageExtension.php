<?php
/**
 */
class MultiImagePageExtension extends DataExtension {
	/* ---- Static variables ---- */
	public static $default_page_class = 'Page';
	public static $default_image = '';
	public static $multi_image_limit = 100;
	public static $multi_image_field = true;
	public static $multi_image_types = array();
	public static $multi_image_positions = array();
	
	public static $default_slider = 'flexslider';	// or 'cycle'
	public static $default_options = array(
		'selector' => '.miholder .miitem',
		'animation' => 'fade',
		'directionNav' => false,
		'controlNav' => false
	);
	
	private static $db = array(
		'MultiImagesPosition' => 'Varchar(30)',
		'MultiImagesType' => 'Varchar(30)'
	);
	
	private static $has_many = array(
		'MultiImages' => 'MultiImage'
	);
	
	/* ---- Instance variables ---- */
	protected $slider;
	protected $sliderOptions;
	
	/* ---- Static methods ---- */
	public static function mi_types_map() {
		$map = array();
		if(empty(self::$multi_image_types)) return $map;
		foreach(self::$multi_image_types as $k => $v) {
			$map[$k] = !empty($v['Title']) ? $v['Title'] : $k;
		}
		return $map;
	}
	
	public static function mi_positions_map() {
		$map = array();
		if(empty(self::$multi_image_positions)) return $map;
		foreach(self::$multi_image_positions as $k => $v) {
			$map[$k] = !empty($v['Title']) ? $v['Title'] : $k;
		}
		return $map;
	}
	
	public static function get_default_mi_type() {
		if(is_array(self::$multi_image_types)) {
			reset(self::$multi_image_types);
			$default = key(self::$multi_image_types);
		}
		return (!empty($default)) ? $default : 'default';
	}
	
	public static function set_multi_image_limit($num,$class=null) {
		if(!$class) $class = self::$default_page_class;
		Config::inst()->update($class,'multi_image_limit',intval($num));
	}
	
	/* ---- Instance methods ---- */
	
	function contentcontrollerInit($controller) {
		Requirements::themedCSS('MultiImageShared');
		switch(self::$default_slider) {
			case 'cycle':
				Requirements::javascript('tkimultiimage/thirdparty/dev/jquery.cycle.all.js');
				Requirements::javascript('tkimultiimage/javascript/multiimage-init-cycle');
			case 'flexslider':
			default:
				Requirements::themedCSS('flexslider');
				Requirements::javascript('tkimultiimage/thirdparty/flexslider/dev/jquery.flexslider.js');
				Requirements::customScript('
					;(function($) {
						$(window).load(function() {
							$(".micycle").flexslider('. $this->JsonSliderOptions() .')
						});
					})(jQuery);
				','micycleflexinit');
				break;
		}
	}
	
	function updateCMSFields(FieldList $fields) {
			// Add multi-images field
		if($this->showMultiImageField() && !$fields->dataFieldByName('MultiImages')) {
			$image_mgr = new GridField(
				'MultiImages',
				'MultiImage',
				MultiImage::get()
			);
			if($this->owner->MultiImages()->count() > $this->getMultiImageLimit()) {
				//$image_mgr->removePermission('add');
			}

			//$image_mgr->copyOnImport = false; // So the darn thing doesn't copy uploads to default upload folder when pressing the 'continue' button!
			//$image_mgr->setAddTitle('image');
			$fields->addFieldToTab('Root.Main',$image_mgr);
			
			// Position
			$positionsMap = self::mi_positions_map();
			if(!empty($positionsMap)) {
				$fields->addFieldToTab('Root.Main', new DropDownField('MultiImagesPosition',_t('MultiImagePageExtension.MULTIIMAGESPOSITION','Position of images'),$positionsMap));
			}
			// Image type
			$typesMap = self::mi_types_map();
			if(!empty($typesMap)) {
				$fields->addFieldToTab('Root.Main', new DropDownField('MultiImagesType',_t('MultiImagePageExtension.MULTIIMAGESTYPE','Image display type'),$typesMap));
			}
			$fields->addFieldToTab('Root.Main', new NumericField('MultiImagesHeight',_t('MultiImagePageExtension.MULTIIMAGESHEIGHT','Crop to height (pixels)')));
		}
	}
	
	public function getMultiImageLimit() {
		$limit = (int) $this->owner->stat('multi_image_limit');
		return (!empty($limit)) ? $limit : self::$multi_image_limit;
	}
	
	/* Checks configuration to determine whether the general multi-image field
	 * is included in the page form. The global decorator static is overridden
	 * by the static property of classes, set by Config::inst()->update();
	 * @return bool 
	 */
	public function showMultiImageField() {
		$stat = $this->owner->stat('multi_image_field');
		return ($stat !== null && is_bool($stat)) ? $stat : (bool) self::$multi_image_field;
	}
	
	/* ---- Slider ---- */
	public function setSlider($name) {
		if(in_array($name,array('flexslider','cycle'))) $this->slider = $name;
	}
	
	public function getSlider() {
		return !empty($this->slider) ? $this->slider : self::$default_slider;
	}
	
	public function setSliderOptions($opts,$merge=true) {
		if(is_array($opts)) {
			$this->sliderOptions = ($merge) ? array_merge(self::$default_options,$opts) : $opts;
		}
	}
	
	public function JsonSliderOptions() {
		return !empty($this->sliderOptions) ? json_encode($this->sliderOptions) : json_encode(self::$default_options);
	}
	
	/* ---- Template methods ---- */
	public function DisplayMultiImages($componentName='MultiImages',$presetName=null) {
			// Get preset specs
		if(MultiImageExtension::preset_exists($presetName)) {
			$preset = MultiImageExtension::get_preset($presetName);
		} else {
			$preset = MultiImageExtension::get_default_preset();
		}
		//if(!is_array($preset) || empty($preset)) user_error('No multi image preset',E_USER_WARNING);
		$specs = MultiImageExtension::get_image_args($preset);
		
			// Instance height - override preset
		$heightField = $componentName .'Height';
		if($this->owner->hasField($heightField)) {
			$specs['height'] = $this->owner->getField($heightField);
			$specs['cropped'] = true;
		}
			// Instance output type
		$typeField = $componentName .'Type';
		if($this->owner->hasField($typeField)) {
			$type = $this->owner->getField($typeField);
		} else {
			$default = self::get_default_mi_type();
			$type = (!empty($default)) ? $default : 'default';
			if(empty($type)) user_error('No default multi image output type',E_USER_WARNING);
		}
		
		$sort = ($type === 'random') ? 'RAND()' : null;
		$mImages = $this->owner->getComponents($componentName,null,$sort);
		if(!$mImages || !$mImages->exists()) return null;
			// Create output
		$dataSet = new DataList();
		$i = 1;
		foreach($mImages as $mImage) {
			if ($type === 'random' && $i > 1) break;
			$img = $mImage->Image();
			if(!$img || !$img->exists()) continue;
			$resampled = $img->MiImage($specs);
			if(!$resampled) continue;
			$resW = (int) $resampled->getWidth();
			$resH = (int) $resampled->getHeight();
				// Title and alt
			if($mImage->Title) {
				$imgTitle = $mImage->Title;	// MultiImage title
			} elseif($img->Title) {
				$imgTitle = $img->Title;	// File title
			} elseif (preg_match("/([^\/]*)\.[a-zA-Z0-9]{1,6}$/", $img->Filename, $matches)) {
				$imgTitle = $matches[1];	// File name
			} else {
				$imgTitle = $img->Filename;
			}
			$imgTitle = Convert::raw2att($imgTitle);
				// Item classes
			$miClasses = 'miitem mipos-'. $mImage->Pos();
			if(!empty($mImage->Title)) {
				$miClasses .= ' miitemwtitle';
			}
			if(!empty($mImage->Caption)) {
				$miClasses .= ' miitemwcaption';
			}
			if(!empty($mImage->Credit)) {
				$miClasses .= ' miitemwcredit';
			}
			$miClasses = Convert::raw2att($miClasses);
			$customisedObj = $mImage->customise(array(
				'MiItemClasses' => $miClasses,
				'ImageSrc' => $resampled->URL,
				'ImageAlt' => $imgTitle,
				'ImageTitle' => $imgTitle,
				'ImageWidth' => ($resW) ? $resW : null,
				'ImageHeight' => ($resH) ? $resH : null
			));
			
			$dataSet->push($customisedObj);
			++$i;
		}
		
			// Template details
		$template = 'Mi' . ucwords($type);
		$viewer = new SSViewer(array($template,'MiDefault'));
		$output = $viewer->process(new ArrayData(array(
			'MultiImages' => $dataSet
		)));
		
		return $output;
	}
	
	public function FirstMultiImage($type='MultiImages') {
		$imageSet = $this->owner->$type();
		$first = $imageSet->First();
		return ($first) ? $first->Image() : null;
	}
	
	public function RandomMultiImage($type='MultiImages') {
		$imgSet = $this->owner->$type();
		$imgArr = $imgSet->toArray();
		if(!$imgSet || !$imgArr || count($imgArr) < 2) return null;
		$max = count($imgArr) - 1;
		$randObj = $imgArr[rand(0,$max)];
		return ($randObj) ? $randObj->Image() : null;
	}
	
	public function FirstChildImage($type='MultiImages') {
		$out = null;
			// Find all children - stage/live adjusted automatically by Versioned
		$children = $this->owner->AllChildren();
		$first = null;
			// Iterate through children and find the first image file
		if($children->exists()) {
			foreach($children as $child) {
				$imageSet = $child->$type();
				$first = $imageSet->First();
				if($first) {
					$out = $first->Image();
					if($out) break;
				}
			}
		}
		return $out;
	}
	
	public function DefaultImage() {
		$out = null;
		if(self::$default_image) {
			$out = File::find(self::$default_image);
		}
		return $out;
	}

}



