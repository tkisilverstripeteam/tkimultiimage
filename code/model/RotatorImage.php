<?php

class RotatorImage extends DataObject {
	
	public static $has_one = array(
		'Image' => 'Image',
		'Page' => 'Page'	// Choose object
	);
	
	public static $singular_name = 'RotatorImage';
	public static $plural_name = 'RotatorImages';
	
	public static function getCMSFields_forPopup() {
		return new FieldList(
			
		);
	}
}

?>