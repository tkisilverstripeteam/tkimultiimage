<?php

class MultiImage extends DataObject {
	/* -------- Static variables -------- */
	public static $db = array(
		"Title" => "Varchar(255)",
		'Caption' => 'Text',
		'Credit' => 'Varchar(255)'
	);
	
	public static $has_one = array(
		'Image' => 'Image',
		'Page' => 'Page'
	);

	public static $has_many = array();

	public static $many_many = array();

	public static $belongs_many_many = array();
	
	public static $extensions = array("Versioned('Stage', 'Live')");
	
	public static $singular_name = 'Multi Image';
	public static $plural_name = 'Multi Images';
	
	/* -------- Instance variables -------- */
	
	/* -------- Static methods -------- */
	public static function getCMSFields_forPopup() {
		$fields = new FieldList(
			new TextField('Title',_t('MultiImage.TITLE', 'Title')),
			new TextareaField('Caption',_t('MultiImage.CAPTION', 'Caption'),4),
			new TextField('Credit',_t('MultiImage.CREDIT', 'Credit'))
		);
		return $fields;
	}
	/* -------- Instance methods -------- */
	
	/* ---- Permissions ---- */
/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canEdit($member = null) {
		$page = $this->Page();
		if($page && $page->exists()) {
			return $page->canEdit($member);
		} else {
			return false;
		}
	}

	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canDelete($member = null) {
		return $this->canEdit($member);
	}

	/**
	 * @param Member $member
	 * @return boolean
	 */
	public function canCreate($member = null) {
		return $this->canEdit($member);
	}
	
	/* ---- Template methods ---- */
	public function HasTitleOrCaption() {
		return (!empty($this->Title) || !empty($this->Caption));
	}
	
	public function HasText() {
		return ($this->HasTitleOrCaption() || !empty($this->Credit));
	}
	
}

?>