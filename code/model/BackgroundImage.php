<?php

class BackgroundImage extends DataObject {
	
	public static $has_one = array(
		'Image' => 'Image',
		'Page' => 'Page'
	);
	
	public static $extensions = array("Versioned('Stage', 'Live')");
	
	public static $singular_name = 'Background Image';
	public static $plural_name = 'Background Images';
	
	public static function getCMSFields_forPopup() {
		return new FieldList(
			
		);
	}
}

?>